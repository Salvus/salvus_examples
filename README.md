# Salvus examples and tutorials

[![build status](https://gitlab.com/Salvus/salvus_examples/badges/master/build.svg)](https://gitlab.com/Salvus/salvus_examples/commits/master)

A number of tutorials for how to use `Salvus`, including all its packages from
mesh generation to forward and inverse modeling.

The Jupyter notebooks are automatically tested against the latest repository
versions of all the packages. Thus this is also serves as a fairly strong
integration test suite and gives a guarantee that the tutorials are up to date.

If you add new things, please use Python >= 3.5 and run

```bash
$ python normalize_notebooks.py
```

before committing.

## Testing

The testing tests the output of all cells against whatever output the notebook
has been saved with. You can add some tags to cells (View->Cell Toolbar->Tags)
to steer the testing behavior. Testing is done by
[nbval](https://github.com/computationalmodelling/nbval) and the following
special tags exist:

* `nbval-skip`: Ignore cell for tests. Don't even execute it.
* `nbval-raises-exception`: This cell is intended to raise an exception.
* `nbval-ignore-output`: Execute cell but ignore the output.

To run the tests, set the `SALVUS_BIN` and `PARAVIEW_BIN` environment variables
to their corresponding executables (`PARAVIEW_BIN` does not actually need to
exist) and run

```bash
SALVUS_BIN=/path/to/salvus PARAVIEW_BIN=/path/to/paraview sh run_tests.sh
```

this might take a while. To get rid of all test generated output (make sure you
don't have any non-version controlled files laying around!):

```bash
$ git clean -fd
```
