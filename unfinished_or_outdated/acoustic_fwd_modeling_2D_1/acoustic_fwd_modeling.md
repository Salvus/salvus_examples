## Acoustic modeling tutorial

This is a `salvus` tutorial aimed at working through a practical forward modeling example. It is split into several parts.

1. Taking the velocity model from a Matlab file to something `salvus_mesher` can read.
2. Building the mesh and model using `salvus_mesher`.
3. Preparing and visualizing the experimental setup, including mesh geometry, sources, and receivers.

Note that all model files and scripts are respectively included in the `data/` and `src/` directories.

# Preparing the velocity model and experiment geometry.

With the code `src/model_processing.py`, we load the file from it's `.mat` container, and visualize it.

![Gullfaks model](gullfaks_model.png)

With the data in python, we can now generate a mesh with this model attached, which is needed by `salvus`. Running the file `src/build_mesh.py` will create a mesh and attach the model. 

![Gullfaks model with mesh](gulfaks_mesh.png)

We accomplish the meshing using the python meshing API defined in `salvus_mesher`. This library allows us to define a mesh by discontinuities, and vertical boundaries. We also can take advantage of SciPy interpolation tools to apply our rectilinear model to our final unstructured mesh, which takes advantage of varying element sizes.

In this mesh we set a coarsening interface at 1124m below the surface, which saves a number of elements due to the 2x wavespeed at depth relative to the surface.

![Gullfaks model with mesh coarsening](gullfaks_mesh_coarsening.png)

Finally, we save the mesh to exodus format, which is the format used by `salvus` for meshes and models.

# Running forward models with mesh.


    mpirun -n 4 ./salvus --mesh-file /scratch/example/moritz_example.e --model-file /scratch/example/moritz_example.e --polynomial-order 4 --duration 10 --time-step 1e-2 --save-movie --movie-file-name ./test.h5 --number-of-sources 2 --source-type ricker --ricker-amplitude 100,100 --ricker-time-delay 1.0,1.5 --ricker-center-freq 0.5,0.5 --source-location-x 50000,90000 --source-location-y 50000,90000 --movie-field u












