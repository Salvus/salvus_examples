import numpy as np
from scipy.io import loadmat
import matplotlib.pyplot as plt

def model_processing(vp_model_file,rho_model_file):
    
    # model assumes 2 meter grid spacing
    model_vp = loadmat(vp_model_file)["gf_vp"]
    model_rho = loadmat(rho_model_file)["gf_rho"]

    spacing = 2 # in meters
    ny = model_vp.shape[0] # note y,x
    nx = model_vp.shape[1]
    xmax = 2*(nx-1);
    ymax = 2*(ny-1);
    
    return (model_vp,model_rho,(0,xmax),(0,ymax))
    
    # lets assume the model starts at x(0) = 0, and uses y in the depth direction, with and inward normal
    # (0,0)                                                                                           x
    #  ------------------------------------------------------------------------------------------------>
    #  |
    #  |
    #  |    .....................................................................................
    #  |
    #  |
    #  |
    #  |
    #  |                                           .............
    #  |                                   .........           . .....           ........
    #  |     ... .. .......................                     .. ...................  ............
    #  |                                               .. ... .
    #  |                                             ..
    #  |                                          ..                     .................
    #  |                                                       ....   ............       .......
    #  |                      ........... . .........     . .... ......        ...... ...   ........
    #  |               ... ....   ............      .......    ...          ...
    #  |      . ......       ......                                         .
    #  |      .         . ..
    #  |                                                    ......                 ...............
    #  |                                           ... ......    . .....     ......
    #  |                                     .......                   ......
    #  |                              . .....
    #  |                          .. .
    #  |                        .
    #  |
    #  |
    # \/ y



def visualize_model():
    (model_vp,model_rho,
     (xmin,xmax),(ymin,ymax)) = model_processing("../data/gullfaks_vp.mat","../data/gullfaks_rho.mat")
    
    print("max/min vp: {}/{}".format(np.max(model_vp),np.min(model_vp)))
    print("model xmax: {}, ymax: {}".format(xmax,ymax))
    
    # visualize mesh
    fig = plt.figure()
    ax = fig.gca()
    cax = ax.imshow(model_vp,extent=(0,xmax,ymax,0))
    plt.xlabel("x - meters")
    plt.ylabel("y - meters depth")
    fig.colorbar(cax)
    plt.draw()
    plt.pause(0.001)

