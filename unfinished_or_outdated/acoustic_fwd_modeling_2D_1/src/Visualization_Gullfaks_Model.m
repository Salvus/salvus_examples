%%% Script to Visualize Gullfaks Model
% Moritz Mueller
% modified 04.08.2016

clear all
close all
clc

%% Load Gullfaks VP Model
filename = sprintf('gullfaks_vp.su');
[gf_vp,SuTraceHeaders,SuHeader]=ReadSu(filename);

filename = sprintf('gullfaks_rho.su');
[gf_rho,SuTraceHeaders,SuHeader]=ReadSu(filename);

%% Save as Matlab File
% filename_vp = sprintf('gullfaks_vp.mat');
% filename_rho = sprintf('gullfaks_rho.mat');
% save(filename_vp,'gf_vp');
% save(filename_rho,'gf_rho');

%% Parameters
[nz nx] = size(gf_vp);

dx = 2;
dz = 2;

xax = (0:nx-1)*dx;
zax = (0:nz-1)*dz;

%% Plotting of VP Model

figure(1)
fsize = 18;
imagesc(xax',zax,gf_vp)
    title('Gullfaks Velocity Model')
    xlabel('Offset [m]')
    ylabel('Depth [m]')
    xlabel('Offset (m)','FontSize',fsize)
    ylabel('Depth (m)','FontSize',fsize)
    set(gca, 'XAxisLocation','top')
    caxis([1500 3200])
    cmap=jet(200);
    colormap(cmap)
    cb = colorbar('vert');
    zlabel = get(cb,'ylabel');
    set(zlabel,'String','VP (m/s)','FontSize',fsize)
    set(gca,'FontSize',fsize)
    
%% Plotting of Density Model

figure(2)
fsize = 18;
imagesc(xax',zax,gf_rho)
    title('Gullfaks Velocity Model')
    xlabel('Offset [m]')
    ylabel('Depth [m]')
    xlabel('Offset (m)','FontSize',fsize)
    ylabel('Depth (m)','FontSize',fsize)
    set(gca, 'XAxisLocation','top')
    caxis([1500 3200])
    cmap=jet(200);
    colormap(cmap)
    cb = colorbar('vert');
    zlabel = get(cb,'ylabel');
    set(zlabel,'String','VP (m/s)','FontSize',fsize)
    set(gca,'FontSize',fsize)