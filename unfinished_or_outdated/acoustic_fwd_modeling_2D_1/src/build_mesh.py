import matplotlib.pyplot as plt
import numpy as np
import sys

# local path to salvus_mesher
sys.path.insert(0, '/home/rietmann/Dropbox/PostDoc/salvus_mesher')

from pymesher.structured_grid_2D import StructuredGrid2D

from model_processing import model_processing

(model_vp,model_rho,
 (xmin,xmax),(ymin,ymax)) = model_processing("../data/gullfaks_vp.mat","../data/gullfaks_rho.mat")

from pymesher.skeleton import Skeleton

from scipy.interpolate import RectBivariateSpline

### Build mesh ###
# define 0 as top. Depth is y-negative.
# 1124 is deepest layer transition. We save some elements by coarsening by factor 2 as velocity is roughly 2x as compared to surface.
discontinuities = np.array([-1500., -1124, 0.])

# coarsening at the bottom where velocity is higher
hmax = np.array([4.0, 2.0])

horizontal_boundaries = (np.asarray([500.]), np.asarray([3500.0]))

sk = Skeleton.create_cartesian_mesh(discontinuities, hmax,
                                    horizontal_boundaries=horizontal_boundaries)
m = sk.get_unstructured_mesh()

### Build velocity model on mesh ###

# get mesh and model x,y points
mesh_x = m.points[:,0]
mesh_y = m.points[:,1]
model_x = np.linspace(xmin,xmax,model_vp.shape[1])
model_y = np.linspace(-ymax,ymin,model_vp.shape[0])

# using SciPy rectilinear interpolation scheme for our model, which is on a regular grid.
vp_model_interpf = RectBivariateSpline(model_x,model_y,np.flipud(model_vp).transpose())
rho_model_interpf = RectBivariateSpline(model_x,model_y,np.flipud(model_rho).transpose())

vp_model_on_mesh_points = np.zeros(m.npoint)
rho_model_on_mesh_points = np.zeros(m.npoint)
for i in range(0,m.npoint):
    # does interpolation
    vp_model_on_mesh_points[i] = vp_model_interpf(mesh_x[i],mesh_y[i])
    rho_model_on_mesh_points[i] = rho_model_interpf(mesh_x[i],mesh_y[i])

# attach our model to the actual mesh    
m.attach_field("VP",vp_model_on_mesh_points)
m.attach_field("RHO",rho_model_on_mesh_points)

# write to exodus for importing into SALVUS
m.write_exodus("/scratch/salvus/moritz_example.e")
