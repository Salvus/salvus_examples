#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Helper function to normalize all notebooks. Currently this means to make sure
the "kernelspec" and "language_info" fields are consistent across all
notebooks.

This is important to get the notebooks tests to execute correctly.

Requires Python 3.5+
"""
import json
from pathlib import Path

notebooks = (Path("./tutorials")).glob("**/*.ipynb")

KERNELSPEC = {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }

LANGUAGE_INFO = {
    "codemirror_mode": {"name": "ipython", "version": 3},
    "file_extension": ".py",
    "mimetype": "text/x-python",
    "name": "python",
    "nbconvert_exporter": "python",
    "pygments_lexer": "ipython3"
}

for nb in notebooks:
    with open(nb, "rb") as fh:
        data = json.load(fh)

    data["metadata"]["kernelspec"] = KERNELSPEC
    data["metadata"]["language_info"] = LANGUAGE_INFO

    with open(nb, "wt") as fh:
        json.dump(data, fh, indent=1)
