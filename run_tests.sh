set -e

if [ -z "$SALVUS_BIN" ]; then
    echo "Need to set SALVUS_BIN"
    exit 1
fi

if [ -z "$PARAVIEW_BIN" ]; then
    echo "Need to set PARAVIEW_BIN"
    exit 1
fi

export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

python normalize_notebooks.py
py.test --nbval --sanitize-with=sanitize_outputs tutorials
