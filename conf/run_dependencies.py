import json
import os
import sys
import time 
from pprint import pprint


projectId=int(sys.argv[1])
projectToken=sys.argv[2]
privateToken=sys.argv[3]
branch=sys.argv[4]

retvalue = os.popen("curl -X POST -F token=%s -F ref=master -F \"variables[MESHER_BRANCH]=%s\" https://gitlab.com/api/v4/projects/%i/trigger/pipeline >>pipeline_%i.json || echo 'HTTP 500' " % (projectToken, branch, projectId, projectId)).readlines()

with open('pipeline_%i.json' % projectId) as data_file:    
	data = json.load(data_file)

id=data["id"]

while True:
	time.sleep(30)  # Delay for 30 seconds
	retvalue = os.popen("curl -X GET --header 'PRIVATE-TOKEN: %s' 'https://gitlab.com/api/v4/projects/%i/pipelines/%i' >>pipeline_%i.json " % (privateToken, projectId, id, id))
	time.sleep(30)  # Delay for 30 seconds
	with open("pipeline_%i.json" % id) as data_file:    
		data = json.load(data_file)
	print(data["status"])
	os.popen("rm pipeline_%i.json" % id)
	if data["status"] == 'success' :
		sys.exit(0)
	if data["status"] == 'failed' :
		sys.exit(1)
