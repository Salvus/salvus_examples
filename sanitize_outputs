# Normalize memory addresses.
[MemoryAddresses]
regex: 0x[a-z0-9]{9,12}
replace: [[MEMORY_ADDRESS]]

# Normalize the runtime.
[TimeLoop]
regex: Time loop completed in \d+\.\d+ seconds.
replace: Time loop completed in X seconds.

# Normalize the progress indicator.
[TimeLoopProgress]
regex: Time loop progress \[\d{1,3}%\].
replace: Time loop progress [X%].

# Normalize the Salvus headers.
[SalvusHeaders]
regex: Salvus version .*
replace: Salvus version X

regex: Floating point size: \d{2}
replace: Floating point size: X

regex: Compiled for GLL orders: .*
replace: Compiled for GLL orders: x

# Carriage return followed by newline - no clue why this happens
# nor while it is not filtered out by default.
# Might be due to the bash output and how the jupyter notebook handles it.
[CarriageReturn]
regex: \r\n
replace: \n

# The mesher can also be fairly verbose
[Mesher]
regex: SUCCESSFULLY GENERATED MESH IN \d+.\d+ SECONDS\.
replace: SUCCESSFULLY GENERATED MESH IN X SECONDS.

# Output filesize of the automated mesher.
regex: SAVED TO ('.*') \(\d+\.\d+ \S+\)\.
replace: SAVED TO \g<1>.

# Replace the beginning of the `run_salvus.sh` which contains the salvus path.
regex: [\/\w]+ --dimension
replace: SALVUS_BIN --dimension=

# Output from salvus seismo
[SalvusSeismo]
regex: \[DONE in \d+\.\d+ seconds\]
replace: [DONE in X seconds]

# Output from salvus seismo for stations outside of the domain.
# The stations can appear in different order, so we just check the total number.
regex: Could not locate station .*
replace: Could not locate station XX

# Try to get rid of all high-precision numbers.
[HighPrecision]
regex: \.(\d{3})\d+
replace: .\g<1>

# Truncate numbers that do not have a leading zero to 2 places after the comma.
regex: ([1-9]+[0-9]*\.\d{2})\d+
replace: \g<1>

[hdf5stuff]
regex: \d+ logical bytes
replace: X logical bytes

regex: \d+ allocated bytes
replace: X allocated bytes

# Normalize 32 and 64 bit floating point data types.
regex: H5T_IEEE_F\d+LE
replace: H5T_IEEE_FXLE

# Normalize simple dataspaces
regex: DATASPACE\s+SIMPLE\s+\{\s*\(\s*\d+\s*\)\s*\/\s*\(\s*\d+\s*\)\s*\}
replace: DATASPACE  SIMPLE { ( X ) / ( X ) }

# Location in output of h5ls
regex: Location:\s*\d+:\d+
replace: Location: X:X

# Datatype statement in h5ls
regex: native\s+(double|float)
replace: native double/float
